.. _specs.getlino:

=========
Reference
=========

List of :cmd:`getlino` commands:

.. toctree::
   :maxdepth: 1

   configure
   startsite
   list
   startproject
