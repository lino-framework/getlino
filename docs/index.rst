.. _getlino:

================================
``getlino`` : the Lino installer
================================

Welcome to the technical documentation about :ref:`getlino`.

Note: If you **just want to install** Lino, then this documentation tree is
**not for you**. It is for people who want to help with developing the tool that
you use to install Lino.Instructions for installing Lino are documented in
different places depending on what you want to do:

===============================  ================================
To install a...                   Read instructions at...
===============================  ================================
:term:`developer environment`     :ref:`getlino.install.dev`
:term:`production server`         :ref:`getlino.install.prod`
:term:`demo server`               :ref:`getlino.install.demo`
===============================  ================================

.. py2rst::

  from getlino import SETUP_INFO
  print(SETUP_INFO['long_description'])

.. rubric:: Table of content

.. toctree::
   :maxdepth: 1

   ref/index
   api/index
   docker
   misc
   changes/index
   ref/db
   ref/misc

.. toctree::
   :hidden:

   install
   usage
   copyright
