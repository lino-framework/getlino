SETUP_INFO = dict(
    name='getlino',
    version='24.11.0',
    install_requires=[
        'click', 'virtualenv', 'jinja2', 'distro', 'synodal',
        'GitPython', 'rstgen'
    ],
    # tests_require=['docker', 'atelier'],
    # test_suite='tests',
    description=
    "A command-line tool for installing Lino in different environments.",
    long_description="""
``getlino`` is a command-line tool for installing Lino in different environments.

- Project homepage: https://gitlab.com/lino-framework/getlino
- Documentation:
  https://getlino.lino-framework.org/
  (mirrored at https://lino-framework.gitlab.io/getlino/)

    """,
    author='Rumma & Ko Ltd',
    author_email='info@lino-framework.org',
    url="https://gitlab.com/lino-framework/getlino",
    license_files=['COPYING'],
    entry_points={'console_scripts': ['getlino = getlino.cli:main']},
    classifiers="""\
Programming Language :: Python :: 3
Development Status :: 5 - Production/Stable
Environment :: Console
Framework :: Django
Intended Audience :: Developers
Intended Audience :: System Administrators
License :: OSI Approved :: GNU Affero General Public License v3
Operating System :: OS Independent
Topic :: System :: Installation/Setup
Topic :: Software Development :: Libraries :: Python Modules
""".splitlines())

SETUP_INFO.update(zip_safe=False, include_package_data=True)

SETUP_INFO.update(packages=[n for n in """
getlino
""".splitlines() if n])
