#!/bin/bash
# generated by getlino
# Copyright 2015-2023 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
#
# Update the application source code in this environment. Run either `pip
# --update` or `git pull` depending on the options specified during startsite.
# Also remove all `*.pyc` files in these repositories.

set -e
umask 0007

PRJDIR=`pwd`
ENVDIR={{envdir}}
REPOS=$ENVDIR/{{repos_link}}

function pull() {
    repo=$REPOS/$1
    cd $repo && pwd && git pull --rebase && cd -
    find -name '*.pyc' -exec rm -f {} +
    cd $PRJDIR
}

. $ENVDIR/bin/activate
LOGFILE=$VIRTUAL_ENV/freeze.log
echo "Run pull.sh in $PRJDIR ($VIRTUAL_ENV)" >> $LOGFILE
date >> $LOGFILE
python -m pip freeze >> $LOGFILE
python -m pip install -U pip

{% if dev_packages -%}
{% for name in dev_packages.split() -%}
pull {{name}}
{% endfor %}
{%- endif %}

{% if pip_packages -%}
pip install -U {{pip_packages}}
{%- endif %}

# How to manually update all packages of the venv:
# pip install -U -r <(pip freeze | sed 's/==/>=/g')
# https://stackoverflow.com/questions/2720014/how-to-upgrade-all-python-packages-with-pip
# pip list --outdated --format=freeze | grep -v '^\-e' | cut -d = -f 1  | xargs pip install -U
# pip list --outdated --format=freeze | awk 'NR>2 {print $1}' | xargs -n1 pip install -U
# ERROR: List format 'freeze' can not be used with the --outdated option.
# ERROR: You must give at least one requirement to install (see "pip help install")

{% if project_dir -%}
touch {{project_dir}}/settings.py
# A pull.sh script in a shared env will not automatically touch all the
# settings.py files.
{%- endif %}
