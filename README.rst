=======================
The ``getlino`` package
=======================




``getlino`` is a command-line tool for installing Lino in different environments.

- Project homepage: https://gitlab.com/lino-framework/getlino
- Documentation:
  https://getlino.lino-framework.org/
  (mirrored at https://lino-framework.gitlab.io/getlino/)

    
